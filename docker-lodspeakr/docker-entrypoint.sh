#!/bin/sh
set -e
if [ -z "$(ls -A "$LODSPEAKR_HOME")" ]; then
	echo "cant find LODSPEAKR_HOME, share folder"

elif [ -z "$(ls -A "$LODSPEAKR_HOME/meta")" ]; then
	yum install -y dos2unix &&  \
	cd $LODSPEAKR_HOME && \
	cp components/default.settings.inc.php settings.inc.php && \
        dos2unix $LODSPEAKR_HOME/utils/create_db.sh && \
	sh utils/create_db.sh db.sqlite && \
	mkdir cache && \
	chown -R apache cache meta && \
	yum remove -y dos2unix
 
fi
exec "$@"
