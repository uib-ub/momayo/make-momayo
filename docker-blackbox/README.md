# Elasticsearch

[Blackbox](https://bitbucket.org/ubbdst/blackbox) elasticsearch docker image. The image is supposed to be run in the 
composed environment with Docker Fuseki and Docker Blackbox in place. 

The image depends on Docker Fuseki and Docker ES, hence those must exist before running this one.

# Run

To start a basic container, go the path where docker-compose.yml resides and run

```
docker-compose build 
docker-compose up 
```

## Access

If all is well, Blackbox will be available at `localhost:8080/blackbox`