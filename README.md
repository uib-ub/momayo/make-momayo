# Momayo dockerized

The repository contains 4 Docker services, namely, Docker Fuseki, Docker Elasticsearch, Docker Blackbox and Docker WebApp . 

Containers need to be run in a composed environment to create a development environment same as that we have in production as of today.

At the core of the Momayo infrastructure is the master data file `data.rdf-xml.owl`. The path fo this file is `*-momayo/data/data.rdf-xml.owl`. "*" could be `marcus` or `riksantikvaren` or whatever name your dataset have. The infrastructure is meant to take rdf datasets that use CIDOC-CRM and the extensions found in the onto-momayo ontology.

As an example we use the `marcus-momayo` repository as an example. This repository could be forked and renamed to the name of your dataset or institution. `make-momayo` can also be forked and the `marcus-momayo` submodule replaced with your own `*-momayo` repository.

## Architecture

![Alt text](docker-update-marcus.png?raw=true "Class diagrams")

- These containers share the same network. Therefore, they can communicate one another by calling their service names f.esk `http://fuseki:3030`.
- To run the containers, one must be in the directory where `docker-compose.yml` resides. 

## Init submodules

We need to clone some submodules.

```bash
git submodule update --init --recursive
```

* `elasticsearch-indexing-files` is needed to index data.
* `onto-momayo` is the Protégé 3.5 project for our ontology
* `marcus-momayo` is the Protégé 3.5 project that contains the following submodules
    * `onto-momayo` the ontology also included one level up
    * `form-momayo` for the form settings for Protégé
    * an empty `data` folder that takes a file called `data.rdf-xml.owl` that is the master data file. 


```bash
# Build the environment
docker-compose build

# Run the environment
docker-compose up

# Destroy the environment
docker-compose down
```

## Running Docker fuseki
All other docker containers depends on Docker Fuseki. By default, fuseki uploads data and creates TDB that are then being copied to Elasticsearch Docker. Sometimes user might not want to load these data everytime when docker starts, this can now be achieved by setting the value of `"LOAD_FUSEKI_DATA_ON_START"` to empty in the file `.env`

## The webapp

The webapp use [LODspeakr](https://github.com/alangrafu/lodspeakr). The setup is at the moment somewhat cumbersome and need to be improved.

LODspeakr uses SPARQL and queries the fuseki container. Any frontend that can use SPARQL could replace LODspeakr. An Angular.io app with an custom Express API would make a much better frontend.

## Access

If all is well, Blackbox will be available at `localhost:8080/blackbox`, Elasticseach will be available at `localhost:9200` and Fuseki 
at `localhost:3030`

## Data injection to Elasticsearch

To index data, you will have to run scripts [elasticsearch-indexing-files](https://bitbucket.org/ubbdst/elasticsearch-indexing-files).
Note that, since now we are using Admin data, you will have to run the `"marcus-admin"` scripts. 

## Edit data with Protégé

UBB have updated the Protégé 3.5 version to "3.6" with some bug fixes and som custom functionality.

Download windows version here: https://github.com/ubbdst/protege-devel-3.5/releases

For Mac there there are som plugins here (some manual copying necessary: https://github.com/ubbdst/protege-owl-plugin/releases

Open the `*-momayo/form-momayo/form-momayo.pprj` to edit.

