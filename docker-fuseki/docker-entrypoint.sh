#!/bin/bash

#Varibales are defined from .env file
ADMIN_TDB=/data/tdb/admin
FUSEKI_DATABASES=/fuseki/databases
MARCUS_ADMIN_DATABASE=$FUSEKI_DATABASES/marcus-admin
ADMIN_TDB_MARCUS=$ADMIN_TDB/marcus
ADMIN_PASSWORD=pwd123

#Leave this variable empty if you do not want to load Fuseki data on start 
echo "Using variable LOAD_FUSEKI_DATA_ON_START: ${LOAD_FUSEKI_DATA_ON_START}"

if  [[ -n "${LOAD_FUSEKI_DATA_ON_START}" ]]; then
     rm -r $MARCUS_ADMIN_DATABASE
     mkdir -p $MARCUS_ADMIN_DATABASE
     
    /jena-fuseki/tdbloader --loc $MARCUS_ADMIN_DATABASE --graph="http://data.ub.uib.no/graphs/data" /staging/data/*.rdf-xml.owl && \
    /jena-fuseki/tdbloader --loc $MARCUS_ADMIN_DATABASE --graph="http://data.ub.uib.no/graphs/ontologies" /staging/ontology/*.owl

    rm -r ADMIN_TDB_MARCUS
    echo "Copying Fuseki TDBs to ${ADMIN_TDB_MARCUS} ..."
    mkdir -p $ADMIN_TDB
    cp -r $MARCUS_ADMIN_DATABASE $ADMIN_TDB_MARCUS
fi
source /fuseki-docker-entrypoint.sh


